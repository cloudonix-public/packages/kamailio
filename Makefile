ifneq (, $(shell which podman))
DOCKER := podman
else
DOCKER := docker
endif

export BUILDAH_FORMAT = docker

build: build-el9 build-el10

build-el8:
	$(DOCKER) build -f el8.Dockerfile -t kamailio-build-el8 .

build-el9:
	$(DOCKER) build -f el9.Dockerfile -t kamailio-build-el9 .

build-el10:
	$(DOCKER) build -f el10.Dockerfile -t kamailio-build-el10 .

image-el8:
	$(DOCKER) build -t cloudonix/kamailio:el8 -f containers/el-runtime.Dockerfile --build-arg STREAM=8 ./
	$(MAKE clean)

image-el9:
	$(DOCKER) build -t cloudonix/kamailio:el9 -f containers/el-runtime.Dockerfile --build-arg STREAM=9 ./
	$(MAKE clean)

image-el10:
	$(DOCKER) build -t cloudonix/kamailio:el10 -f containers/el-runtime.Dockerfile --build-arg STREAM=10 ./
	$(MAKE clean)

image: image-el8 image-el9

clean:
	$(DOCKER) rmi kamailio-build-el7 kamailio-build-el8 kamailio-build-el9 kamailio-build-el10 || true
	rm -rf ${PWD}/containers/el8 ${PWD}/containers/el9 ${PWD}/containers/el10

.PHONY: build build-el8 build-el9 build-el10 image-el8 image-el9 image-el10 image clean
