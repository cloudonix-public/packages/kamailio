#!/bin/bash

yum install -y mc git && git init && git add kamailio-$VERSION/
git config --global user.name 'Your Name'
git config --global user.email 'Your email'
git commit -m init >/dev/null
(cd kamailio-$VERSION/; for patch in /src-orig/patches/*; do patch -p1 < $patch; done)
