ARG STREAM=9

# ----- Build libphonenumber
FROM almalinux:${STREAM} as libphonebuilder

ARG STREAM
ENV LIBPHONENUMBER_VERSION=8.13.37 LIBPHONENUMBER_REL=1.fc41
ENV SRCRPM_URL=https://kojipkgs.fedoraproject.org/packages/libphonenumber
ENV SRC_VERSION_PATH=$LIBPHONENUMBER_VERSION/$LIBPHONENUMBER_REL/src
ENV SRC_VERSION_NAME=$LIBPHONENUMBER_VERSION-$LIBPHONENUMBER_REL

RUN dnf upgrade -y && dnf install -y rpm-build rpmdevtools yum-utils epel-release libarchive
RUN dnf config-manager --set-enabled epel appstream $([ "$STREAM" == 8 ] && echo powertools || echo crb)
RUN set -x && mkdir -p $(rpm --eval "%{_sourcedir}") $(rpm --eval "%{_specdir}")
# build required deps not in upstream
RUN set -x && rpm -i $SRCRPM_URL/$SRC_VERSION_PATH/libphonenumber-$SRC_VERSION_NAME.src.rpm
RUN dnf builddep -y $(rpm --eval "%{_specdir}")/libphonenumber.spec
RUN dnf install -y make # missing from source package dep list
RUN rpmbuild -ba $(rpm --eval "%{_specdir}")/libphonenumber.spec

# ----- Build kamailio from upstream
FROM almalinux:${STREAM} as kamailiobuilder

ARG STREAM
ENV VERSION=5.7.6 BASE_URL=https://www.kamailio.org/pub/kamailio

RUN dnf upgrade -y && dnf install -y rpm-build rpmdevtools yum-utils epel-release
RUN dnf config-manager --set-enabled epel appstream $([ "$STREAM" == 8 ] && echo powertools || echo crb)
RUN mkdir -p $(rpm --eval "%{_sourcedir}") $(rpm --eval "%{_specdir}")
# install missing required deps not in upstream
COPY --from=libphonebuilder /root/rpmbuild/RPMS/x86_64/libphonenumber* /root/rpmbuild/RPMS/x86_64/
RUN dnf install -y /root/rpmbuild/RPMS/x86_64/libphonenumber-{,devel-}[0-9]*.rpm
# build kamailio
RUN cd $(rpm --eval "%{_sourcedir}") && curl -sf -O ${BASE_URL}/${VERSION}/src/kamailio-${VERSION}_src.tar.gz
WORKDIR /src
RUN tar -xf $(rpm --eval "%{_sourcedir}")/kamailio-${VERSION}_src.tar.gz
ADD prepare.sh /src/prepare
ADD patches /src/patches
RUN set -e && for patch in /src/patches/*; do echo Patching with $patch; (cd kamailio-$VERSION; patch -p1 < $patch); done
RUN cp kamailio-${VERSION}/pkg/kamailio/obs/* $(rpm --eval "%{_sourcedir}")
RUN mv $(rpm --eval "%{_sourcedir}")/kamailio.spec $(rpm --eval "%{_specdir}")
RUN dnf builddep -y -D "_without_nats 1" $(rpm --eval "%{_specdir}")/kamailio.spec
# replace the downloaded source tarball with the one we patched
RUN rm $(rpm --eval "%{_sourcedir}")/kamailio-${VERSION}_src.tar.gz
RUN tar -zcf $(rpm --eval "%{_sourcedir}")/kamailio-${VERSION}_src.tar.gz kamailio-${VERSION}
RUN rpmbuild -ba $(rpm --eval "%{_specdir}")/kamailio.spec --without nats

FROM almalinux:${STREAM}-minimal as kamailioruntime
ARG STREAM
COPY --from=kamailiobuilder /root/rpmbuild/RPMS/x86_64/* /usr/local/share/repository/
RUN microdnf install -y dnf
RUN dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-${STREAM}.noarch.rpm
RUN dnf install -y $(ls /usr/local/share/repository/kamailio{,-geoip,-http_client,-json,-mysql,-outbound,-redis,-rtjson,-sctp,-tls,-tcpops,-utils,-uuid,-websocket,-xmpp}-5*.rpm)

FROM kamailioruntime
VOLUME /etc/kamailio
ENTRYPOINT [ \
    "/usr/sbin/kamailio", \
    "-DD", \
    "--atexit=no", \
    "-P", "/run/kamailio/kamailio.pid", \
    "-f", "/etc/kamailio/kamailio.cfg" \
    ]
