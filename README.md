# kamailio

Build script for Kamailio used in the Cloudonix systems

## Development

To update the patches on the build, do the following:

1. Run `DOCKER_BUILDKIT=0 docker build -f el7.Dockerfile .` (or `el8.Dockerfile`)
2. Find the layer where the patches were added to the container ("ADD patches ...") and record the resulting image ID
3. Run `docker run -ti --rm -v ${PWD}:/src-orig <image-id>`
4. In the container run `./prepare`
5. Use `mcedit` to edit which ever file you want to change
6. Update the relevant patch using `git diff > /src-orig/patches/my-patch-file.patch`
7. Test the build by running `docker build` again

## Rebasing on a new version

When a new Kamailio release comes out, assuming no other changes are required except building the new version,
change the version number in the following files:

- `.gitlab-ci.yml'
- `el7.Dockerfile`, `el8.Dockerfile`
- `patches/spec-local-config.patch`

Then commit the changes and let Gitlab build the new release

## Building a docker image for deployment

To build a docker image for your deployment, do the following:

1. Checkout the kamailio branch you would like to build `5.3`, `5.4`, `5.5`, `5.6` or `5.7`
2. Change to the `containers` directory
3. Run `./docker_build`

Upon completion, you will have a new docker image available on your machine. 

**Note:**\
If you didn't checkout one of the proper branches in `step 1`, the script will automatically default to the current stable branch (`5.6`).

###Copyright
&copy; 2020 - 2021, Oded Arbel, Nir Simionovich and Cloudonix, Inc.




