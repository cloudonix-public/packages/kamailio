FROM quay.io/centos/centos:stream9 as libphonebuilder

ENV LIBPHONENUMBER_VERSION=8.12.11 LIBPHONENUMBER_REL=15.fc37
ENV SRCRPM_URL=https://kojipkgs.fedoraproject.org/packages
ENV SRC_VERSION_PATH=$LIBPHONENUMBER_VERSION/$LIBPHONENUMBER_REL
ENV SRC_VERSION_NAME=$LIBPHONENUMBER_VERSION-$LIBPHONENUMBER_REL

RUN dnf upgrade -y && dnf install -y rpm-build rpmdevtools yum-utils epel-release
RUN dnf config-manager --set-enabled epel appstream crb
RUN mkdir -p $(rpm --eval "%{_sourcedir}") $(rpm --eval "%{_specdir}")
# build required deps not in upstream
RUN rpm -i $SRCRPM_URL/libphonenumber/$SRC_VERSION_PATH/src/libphonenumber-$SRC_VERSION_NAME.src.rpm
RUN yum-builddep -y $(rpm --eval "%{_specdir}")/libphonenumber.spec
RUN dnf install -y make # missing from source package dep list
RUN rpmbuild -ba $(rpm --eval "%{_specdir}")/libphonenumber.spec

FROM quay.io/centos/centos:stream9

ENV VERSION=5.7.6 BASE_URL=https://www.kamailio.org/pub/kamailio

RUN dnf upgrade -y && dnf install -y rpm-build rpmdevtools yum-utils epel-release
RUN dnf config-manager --set-enabled epel appstream crb
RUN mkdir -p $(rpm --eval "%{_sourcedir}") $(rpm --eval "%{_specdir}")
# install missing required deps not in upstream
COPY --from=libphonebuilder /root/rpmbuild/RPMS/x86_64/libphonenumber* /tmp/
RUN dnf install -y /tmp/libphonenumber-{,devel-}[0-9]*.rpm
# build kamailio
RUN cd $(rpm --eval "%{_sourcedir}") && curl -sf -O ${BASE_URL}/${VERSION}/src/kamailio-${VERSION}_src.tar.gz
WORKDIR /src
RUN tar -xf $(rpm --eval "%{_sourcedir}")/kamailio-${VERSION}_src.tar.gz
ADD prepare.sh /src/prepare
ADD patches /src/patches
RUN for patch in /src/patches/*; do (cd kamailio-$VERSION; patch -p1 < $patch); done
RUN cp kamailio-${VERSION}/pkg/kamailio/obs/* $(rpm --eval "%{_sourcedir}")
RUN mv $(rpm --eval "%{_sourcedir}")/kamailio.spec $(rpm --eval "%{_specdir}")
RUN dnf builddep -y -D "_without_nats 1" $(rpm --eval "%{_specdir}")/kamailio.spec
# replace the downloaded source tarball with the one we patched
RUN rm $(rpm --eval "%{_sourcedir}")/kamailio-${VERSION}_src.tar.gz
RUN tar -zcf $(rpm --eval "%{_sourcedir}")/kamailio-${VERSION}_src.tar.gz kamailio-${VERSION}
RUN rpmbuild -ba $(rpm --eval "%{_specdir}")/kamailio.spec --without nats
