FROM centos:7 as libphonebuilder

ENV LIBPHONENUMBER_VERSION=8.12.11 LIBPHONENUMBER_REL=15.fc37
ENV SRCRPM_URL=https://kojipkgs.fedoraproject.org/packages
ENV SRC_VERSION_PATH=$LIBPHONENUMBER_VERSION/$LIBPHONENUMBER_REL
ENV SRC_VERSION_NAME=$LIBPHONENUMBER_VERSION-$LIBPHONENUMBER_REL

RUN yum install -y rpm-build rpmdevtools yum-utils epel-release centos-release-scl
RUN mkdir -p $(rpm --eval "%{_sourcedir}") $(rpm --eval "%{_specdir}")
# install modern build toolchain (libphonenumber requires a full C++11 not available in Centos7, but it is also a good idea)
RUN yum install -y cmake3 devtoolset-10-gcc-c++ && (echo ". /opt/rh/devtoolset-10/enable" >> /etc/profile.d/sh.local)
SHELL ["/bin/sh", "-l", "-c"]
RUN (echo "%cmake_build %cmake3_build"; echo "%cmake_install %cmake3_install") > /usr/lib/rpm/macros.d/macros.cmake
# build required deps not in upstream
RUN rpm -i $SRCRPM_URL/libphonenumber/$SRC_VERSION_PATH/src/libphonenumber-$SRC_VERSION_NAME.src.rpm
# remove broken changelog entry from that one guy
RUN sed -i 's/16:57:48 CET //' $(rpm --eval "%{_specdir}")/libphonenumber.spec
RUN yum-builddep -y $(rpm --eval "%{_specdir}")/libphonenumber.spec
RUN rpmbuild -ba $(rpm --eval "%{_specdir}")/libphonenumber.spec

FROM centos:7

ENV VERSION=5.7.6 BASE_URL=https://www.kamailio.org/pub/kamailio

RUN yum install -y rpm-build rpmdevtools yum-utils epel-release
RUN mkdir -p $(rpm --eval "%{_sourcedir}") $(rpm --eval "%{_specdir}")
# install missing required deps not in upstream
COPY --from=libphonebuilder /root/rpmbuild/RPMS/x86_64/libphonenumber* /tmp/
RUN yum install -y /tmp/libphonenumber-{,devel-}[0-9]*.rpm
# build kamailio
RUN cd $(rpm --eval "%{_sourcedir}") && curl -sf -O ${BASE_URL}/${VERSION}/src/kamailio-${VERSION}_src.tar.gz
WORKDIR /src
RUN tar -xf $(rpm --eval "%{_sourcedir}")/kamailio-${VERSION}_src.tar.gz
ADD prepare.sh /src/prepare
ADD patches /src/patches
RUN for patch in /src/patches/*; do (cd kamailio-$VERSION; patch -p1 < $patch); done
RUN cp kamailio-${VERSION}/pkg/kamailio/obs/* $(rpm --eval "%{_sourcedir}")
RUN mv $(rpm --eval "%{_sourcedir}")/kamailio.spec $(rpm --eval "%{_specdir}")
RUN yum-builddep -y $(rpm --eval "%{_specdir}")/kamailio.spec
RUN rpmbuild -ba $(rpm --eval "%{_specdir}")/kamailio.spec
